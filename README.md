# ZBOP Project

Projekt na przedmiot ZBOP (Zastosowania badań operacyjnych w przedsiębiorstwach)

## Skład zespołu
|                    |              |
| -----------        | -----------  |
| **Przedmiot**      | ZBOP         |
| **Semestr**        | 2021Z        |
| **Projekt**        | Zarządzanie produkcją ciast i ich dystrybucją do cukierni         |
| **Zespół**        | Zespół D         |
| **Skład zespołu**  | <ul><li>Zych Magdalena</li><li>Jarek Maria</li><li>Michał Wiśniewski</li></ul> |

## Opis problemu podstawowego
Celem w modelu podstawowym jest zaplanowanie produkcji ciast w fabrykach tak, aby koszt był jak najmniejszy. Koszty wytwarzania ciast różnią się w zależności od fabryki i są proporcjonalne do ilości wytwarzanego ciasta. Różnice w tych kosztach dla fabryk wynikają z tego, że niektóre zakłady mogą mieć sprzęt, którego użytkowanie wiąże się z większymi opłatami. Wpływa na to także lokalizacja fabryk, od której zależny jest czynsz oraz fakt, że konkretna fabryka może specjalizować się w wytwarzaniu niektórych rodzajów ciast. Dodatkowo naliczana jest opłata za rozpoczęcie produkcji ciast danego typu. Jest to brane pod uwagę, ponieważ przygotowanie do robienia ciast wymaga dodatkowego czasu i pracy. Czynności te są wykonywane jednorazowo i różnią się w zależności od typu ciasta. Przykładowo, przygotowanie i uporządkowanie stanowisk po robieniu tortów wymaga więcej pracy niż po robieniu ciast drożdżowych. Możliwości wytwórcze fabryk są ograniczone. Pod uwagę brane są także koszty transportu z fabryk do cukierni. Przyjęto założenie, że zapotrzebowanie jest znane dla każdej cukierni i musi być spełnione. W modelu podstawowym pominięto możliwość magazynowania niesprzedanych wyrobów.

## Opis problemu złożonego

W modelu rozszerzonym uwzględniona została możliwość magazynowania ciast. Każdy rodzaj ciasta ma zdefiniowaną maksymalną liczbę dni, przez którą może być przechowywany. Magazyny znajdują się przy cukierniach, ale nie muszą być wykorzystywane. Korzystanie z konkretnego magazynu wiąże się z jednorazową opłatą na danym horyzoncie czasowym. Zastosowano dekompozycję modelu, w wyniku której w pierwszej kolejności podejmowane są decyzje taktyczne, a drugiej szczegółowe. 
